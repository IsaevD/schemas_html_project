$(function(){
	initializeMapActions();	
});

function initializeMapActions() {
	$("#plans .plan.active img").maphilight();
	$("#plans .plan")
		.on("mouseout", "area", function(){
			$(".rooms .room").removeClass("active");
		})
		.on("mouseenter", "area", function(){
			$(".rooms .room[data-roomid="+$(this).attr("data-roomid")+"]").addClass("active");
		});
	$("#stages .stage .action").click(function(){
		$("#stages .stage").removeClass("active");
		$("#plans .plan").removeClass("active");
		$(this).parent().addClass("active");
		currentPlan = $("#plans .plan[data-stageid="+$(this).parent().attr("data-stageid")+"]");
		currentPlan.addClass("active");
		currentPlan.children("img").maphilight();
	});
	$(".rooms")
		.on("mouseenter", ".room", function(){
			$("area[data-roomid=" + $(this).attr("data-roomid")+ "]").mouseenter();
		})
		.on("mouseout", ".room", function(){
			$("area[data-roomid=" + $(this).attr("data-roomid")+ "]").mouseout();
		});
}